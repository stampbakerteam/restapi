StampBaker
=========
###Restfull API для сервиса изготовления печатей.
##### Для более четкого понимания[ - ссылка на макет фронтенда](https://stampbaker.herokuapp.com)

Реализовано c использованием "Multitier architecture"

* Слой сериализации(Presentation tier)- EventSubscriber/BaseContoller
* Слой маршрутизации(Application layer) - контроллеры
* Слой бизнес-логики(Business logic layer ) - сервисы
* Слой доступа к данным(Data access layer) - репозитории                

#####Для запуска:
`$ mkdir -p var/jwt`  
`$ openssl genrsa -out var/jwt/private.pem -aes256 4096`  
`$ openssl rsa -pubout -in var/jwt/private.pem -out var/jwt/public.pem`  
`$ composer install`  
`$ doctrine:database:create`  
`$ php bin/console doctrine:migrations:migrate --no-interaction`  
`$ php bin/console  server:start 0.0.0.0:8000`




#####Основной фунционал:

* Регистрация пользователя
* Авторизация пользоватея(JWT токены)
* Упраление пользователями
* Создание заказа на изготовления печати.    
* Управление заказами.


#####Осталось сделать:
* Восстановоение пароля
* Добавление к заказу картинок
* Rest-сервисы для валидации "на лету" регистрационной формы
* Оповещение одминов о создании заказа через email и телеграм
* Защита форм (капча?
)
* TODO в коде)