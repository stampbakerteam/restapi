<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170414104456 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE Orders (id INT AUTO_INCREMENT NOT NULL, user INT DEFAULT NULL, state VARCHAR(255) NOT NULL, created DATETIME NOT NULL, main_type VARCHAR(255) NOT NULL, type_of_manufacture VARCHAR(255) NOT NULL, need_tooling TINYINT(1) NOT NULL, tooling_type VARCHAR(255) DEFAULT NULL, pics_count INT DEFAULT NULL, seal_type VARCHAR(255) DEFAULT NULL, seal_shape VARCHAR(255) DEFAULT NULL, need_integrate_logo TINYINT(1) NOT NULL, seal_fio VARCHAR(255) DEFAULT NULL, seal_company_name VARCHAR(255) DEFAULT NULL, seal_inn VARCHAR(255) DEFAULT NULL, seal_ogrn VARCHAR(255) DEFAULT NULL, seal_city VARCHAR(255) DEFAULT NULL, seal_text1 VARCHAR(255) DEFAULT NULL, seal_text2 VARCHAR(255) DEFAULT NULL, size VARCHAR(255) DEFAULT NULL, height INT DEFAULT NULL, width INT DEFAULT NULL, stamp_text VARCHAR(255) DEFAULT NULL, delivery_type VARCHAR(255) DEFAULT NULL, address VARCHAR(255) DEFAULT NULL, need_prototype TINYINT(1) DEFAULT NULL, is_fetching TINYINT(1) DEFAULT NULL, count INT DEFAULT NULL, payment_type VARCHAR(255) DEFAULT NULL, comment VARCHAR(255) DEFAULT NULL, is_asap TINYINT(1) DEFAULT NULL, INDEX IDX_E283F8D88D93D649 (user), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE UnconfirmedUsers (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(255) NOT NULL, username VARCHAR(255) NOT NULL, full_name VARCHAR(255) NOT NULL, hash VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, phone_number VARCHAR(255) NOT NULL, created DATETIME NOT NULL, UNIQUE INDEX UNIQ_B0096738E7927C74 (email), UNIQUE INDEX UNIQ_B0096738F85E0677 (username), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE Users (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(255) NOT NULL, full_name VARCHAR(255) NOT NULL, role VARCHAR(255) NOT NULL, phone_number VARCHAR(255) NOT NULL, status VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, created DATETIME NOT NULL, updated DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_D5428AEDF85E0677 (username), UNIQUE INDEX UNIQ_D5428AEDE7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE Orders ADD CONSTRAINT FK_E283F8D88D93D649 FOREIGN KEY (user) REFERENCES Users (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE Orders DROP FOREIGN KEY FK_E283F8D88D93D649');
        $this->addSql('DROP TABLE Orders');
        $this->addSql('DROP TABLE UnconfirmedUsers');
        $this->addSql('DROP TABLE Users');
    }
}
