<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170414105058 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        //TODO Switch to fixtures
        $this->addSql('INSERT INTO `stampbaker`.`Users` (`id`, `username`, `full_name`, `email`, `password`,`phone_number`,`created`, `updated`, `role`,  `status`) VALUES (\'1\', \'admin\', \'Admin\', \'admin@admin.com\', \'$2y$13$Ikwr51n/gkxQ9KpaTLpTc.faO18jyQHfaW/tGDTn2qkhUVZ.bbbZe\', \'+79043333333\', \'2017-04-13 14:45:55\', null, \'ROLE_ADMIN\', \'READY\');');
        $this->addSql('INSERT INTO `stampbaker`.`Orders` (`id`, `user`, `state`, `created`, `main_type`, `type_of_manufacture`, `need_tooling`, `tooling_type`, `pics_count`, `seal_type`, `seal_shape`, `need_integrate_logo`, `seal_fio`, `seal_company_name`, `seal_inn`, `seal_ogrn`, `seal_city`, `seal_text1`, `seal_text2`, `size`, `height`, `width`, `stamp_text`, `delivery_type`, `address`, `need_prototype`, `is_fetching`, `count`, `payment_type`, `comment`, `is_asap`) VALUES (\'1\', \'1\', \'CREATED\', \'2017-04-14 14:02:52\', \'TYPE_SEAL\', \'BY_HAND\', \'1\', \'BIG\', \'3\', \'ROUND\', \'ROUND\', \'1\', \'Bruce Wayn\', \'Acme Ltd\', \'123455\', \'1234565\', \'Gotham City\', \'Bathman\', \'Bathman\', \'BIG\', \'100\', \'100\', \'Hmm...\', \'SELF\', \'Gotham, 13, 891111\', \'1\', \'1\', \'1\', \'CASH\', \'Wow\', \'1\');');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {   //TODO
        // this down() migration is auto-generated, please modify it to your needs

    }
}
