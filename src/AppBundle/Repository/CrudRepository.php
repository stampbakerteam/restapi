<?php
/**
 * Created by PhpStorm.
 * User: lex
 * Date: 05.04.17
 * Time: 17:34
 */

namespace AppBundle\Repository;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpKernel\KernelEvents;

class CrudRepository extends EntityRepository
{
    public function save($obj)
    {
        $this->getEntityManager()->persist($obj);

    }


    public function delete($obj)
    {
        $this->getEntityManager()->remove($obj);
    }


    public function update($obj)
    {
        $this->getEntityManager()->merge($obj);
    }

    //Not crud...

    public function findById($id)
    {

        return $this->findOneBy(array(
            'id' => $id
        ));
    }



}