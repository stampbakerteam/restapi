<?php
/**
 * Created by PhpStorm.
 * User: lex
 * Date: 11.04.17
 * Time: 17:11
 */

namespace AppBundle\Repository;


use Doctrine\Common\Collections\Criteria;

class UnconfirmedUserRepository extends CrudRepository
{
    public function getByHash($hash)
    {
        return $this->findOneBy(array('hash' => $hash));
    }

    public function findByEmailOrUsername($email, $username)
    {
        $qb = $this->createQueryBuilder("u");
        $query = $qb
            ->where("u.email LIKE :email")
            ->orWhere("u.username LIKE :username")
            ->setParameters(['email' => $email, 'username' => $username])
            ->getQuery();
        return $query->getResult();
    }


}