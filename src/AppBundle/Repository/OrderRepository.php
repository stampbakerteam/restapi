<?php
/**
 * Created by PhpStorm.
 * User: lex
 * Date: 05.04.17
 * Time: 16:12
 */

namespace AppBundle\Repository;

use AppBundle\Api\Model\OrderState;
use Doctrine\ORM\EntityRepository;

class OrderRepository extends CrudRepository
{
    function findByUserId($id)
    {
        $qb = $this->createQueryBuilder("o");
        $query = $qb
            ->where("o.user == :id")
            ->andWhere("o.state != :state")
            ->setParameters(['id' =>  $id , 'state' => OrderState::DELETED])
            ->getQuery();
        return $query->getResult();
    }


    function getAll()
    {
        $qb = $this->createQueryBuilder("o");
        $query = $qb
            ->andWhere("o.state != :state")
            ->orderBy('o.created', 'ASC')
            ->setParameters(['state' => OrderState::DELETED])
            ->getQuery();
        return $query->getResult();
    }






    function findAllByState($state)
    {
        return  $this->findBy(array(
            'state' => $state
        ));

    }
}