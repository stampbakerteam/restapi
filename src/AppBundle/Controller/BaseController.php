<?php
/**
 * Created by PhpStorm.
 * User: lex
 * Date: 05.04.17
 * Time: 14:55
 */

namespace AppBundle\Controller;

use AppBundle\Api\Serialization\SerializationExclusionStrategy;
use JMS\Serializer\SerializationContext;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;


class BaseController extends Controller implements IBaseController
{
    const EMPTY_RESPONSE = "";

    public function createResponse($data, $statusCode = 200)
    {
        $request = $this->get('request_stack')->getCurrentRequest();
        //TODO Make serialization dependent on the header AcceptType

        //TODO Find way to commit session after serialisation;
        $this->commitORMSession();
        $response = new Response($this->serialize($data, $request->getAcceptableContentTypes()), $statusCode, array(
            'Content-Type' => 'application/json'
        ));

        return $response;
    }

    private function commitORMSession()
    {
        $this->get('doctrine.orm.entity_manager')->flush();
    }

    private function serialize($data, $acceptableContentTypes)
    {
        $context = new SerializationContext();
        $context->addExclusionStrategy(
            new SerializationExclusionStrategy()
        );


        return $this->get('jms_serializer')->serialize($data, 'json', $context);

    }
}