<?php
/**
 * Created by PhpStorm.
 * User: lex
 * Date: 29.03.17
 * Time: 10:59
 */

namespace AppBundle\Controller\Api\v1;


use AppBundle\Controller\BaseController;
use AppBundle\Controller\IBaseController;


use AppBundle\Annotation\DeserializableType;
use AppBundle\Entity\Order;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;


//TODO Make route /api/v1 dynamic

/**
 *
 * @Route("/api/v1/orders")
 *
 *
 * */
class OrderController extends BaseController
{
    /**
     * @Route("/", name="api_v1_all_orders")
     * @Method("GET")
     * @Security("has_role('ROLE_ADMIN')")
     *
     */

    public function getAllAction()
    {

        return $this->createResponse($this->get('api.order_service')->getAllOrders());

    }


    /**
     * @Route("/my", name="api_v1_my_orders")
     * @Method("GET")
     * @Security("has_role('ROLE_USER')")
     *
     */

    public function getMyAction()
    {

        return $this->createResponse($this->get('api.order_service')->getMyOrders());

    }





    /**
     * @Route("/", name="api_v1_create_order")
     * @Method("POST")
     * @DeserializableType(type="AppBundle\Entity\Order")
     *
     */
    public function createAction($order = null)
    {

        return $this->createResponse($this->get('api.order_service')->createOrder($order));

    }


    /**
     * @Route("/{id}", name="api_v1_get_by_id_order", requirements={"id": "\d+"})
     * @Method("GET")
     * @Security("has_role('ROLE_ADMIN')")
     *
     */
    public function getByIdAction($id)
    {

        return $this->createResponse($this->get('api.order_service')->findByIdOrders($id));

    }


    /**
     * @Route("/{id}", name="api_v1_delete_order", requirements={"id": "\d+"})
     * @Method("DELETE")
     * @Security("has_role('ROLE_ADMIN')")
     *
     */
    public function deleteAction($id)
    {
        $this->get('api.order_service')->deleteOrder($id);
        return $this->createResponse(BaseController::EMPTY_RESPONSE);

    }


    /**
     * @Route("/{id}/process", name="api_v1_process_order", requirements={"id": "\d+"})
     * @Method("GET")
     * @Security("has_role('ROLE_ADMIN')")
     *
     */
    public function processAction($id)
    {
        $this->get('api.order_service')->startProcessOrder($id);
        return $this->createResponse(BaseController::EMPTY_RESPONSE);

    }


    /**
     * @Route("/{id}/done", name="api_v1_close_order", requirements={"id": "\d+"})
     * @Method("GET")
     * @Security("has_role('ROLE_ADMIN')")
     *
     */
    public function closeAction($id)
    {
        $this->get('api.order_service')->closeOrder($id);
        return $this->createResponse(BaseController::EMPTY_RESPONSE);

    }
}