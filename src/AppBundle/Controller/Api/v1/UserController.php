<?php
/**
 * Created by PhpStorm.
 * User: lex
 * Date: 29.03.17
 * Time: 10:59
 */

namespace AppBundle\Controller\Api\v1;


use AppBundle\Controller\BaseController;
use AppBundle\Controller\IBaseController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Annotation\DeserializableType;

/**
 *
 * @Route("/api/v1/users", )
 *
 */
class UserController extends BaseController
{

    /**
     * @Route("/", name="api_v1_users")
     * @Security("has_role('ROLE_ADMIN')")
     * @Method("GET")
     *
     */
    public function usersAction()
    {
        return $this->createResponse($this->get('api.user_service')->findAll());
    }

    /**
     * @Route("/me", name="api_v1_users_me")
     * @Security("has_role('ROLE_USER')")
     * @Method("GET")
     */

    public function getMeAction()
    {
        return $this->createResponse($this->get('api.user_service')->getMe());
    }

    /**
     * @Route("/", name="api_v1_users_create")
     * @Security("has_role('ROLE_ADMIN')")
     * @Method("POST")
     * @DeserializableType(type="AppBundle\Entity\User")
     */
    public function createAction($user = null)
    {
        return $this->createResponse($this->get('api.user_service')->createUser($user));
    }

    /**
     * @Route("/", name="api_v1_users_update")
     * @Method("PUT")
     * @Security("has_role('ROLE_ADMIN')")
     * @DeserializableType(type="AppBundle\Entity\User")
     */
    public function updateAction($user = null)
    {
        return $this->createResponse($this->get('api.user_service')->updateUser($user));
    }

    /**
     * @Route("/{id}", name="api_v1_users_delete", requirements={"id": "\d+"})
     * @Security("has_role('ROLE_ADMIN')")
     * @Method("DELETE")
     * @DeserializableType(type="NONE")
     */
    public function deleteAction($id)
    {
        $this->get('api.user_service')->deleteUser($id);
        return $this->createResponse(BaseController::EMPTY_RESPONSE);

    }

    /**
     * @Route("/{id}", name="api_v1_users_user", requirements={"id": "\d+"})
     * @Security("has_role('ROLE_ADMIN')")
     * @Method("GET")
     */
    public function userAction($id)
    {
        return $this->createResponse($this->get('api.user_service')->findById($id));
    }


}