<?php
/**
 * Created by PhpStorm.
 * User: lex
 * Date: 10.04.17
 * Time: 18:01
 */

namespace AppBundle\Controller\Api\v1;


use AppBundle\Api\Model\Credentials;
use AppBundle\Controller\BaseController;
use AppBundle\Annotation\DeserializableType;
use AppBundle\Annotation\QueryParams;
use AppBundle\Controller\IBaseController;
use AppBundle\Api\Model\JWToken;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;

/**
 *
 * @Route("/api/v1/auth", )
 *
 */
class AuthController extends BaseController implements IBaseController
{
    /**
     * @Route("/register", name="api_v1_register")
     * @Method("POST")
     * @DeserializableType(type="AppBundle\Entity\UnconfirmedUser")
     */
    public function registerAction($unconfirmedUser = null)
    {
        $registerStatus = $this->get('api.auth_service')->register($unconfirmedUser);
        return $this->createResponse($registerStatus);
    }

    /**
     * @Route("/confirm", name="api_v1_confirm")
     * @Method("GET")
     * @QueryParams({"hash"="hash"});
     */
    public function confirmRegisterAction($hash = null)
    {
        $token = $this->get('api.auth_service')->confirmRegistration($hash);
        return $this->createResponse(new JWToken($token));
    }

    /**
     * @Route("/login", name="api_v1_login")
     * @Method("POST")
     * @DeserializableType(type="AppBundle\Api\Model\Credentials")
     */
    public function loginAction(Credentials $credentials = null)
    {
        $token = $this->get('api.auth_service')->auth($credentials);
        return $this->createResponse(new JWToken($token));
    }


}