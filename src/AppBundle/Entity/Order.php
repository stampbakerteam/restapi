<?php
/**
 * Created by PhpStorm.
 * User: lex
 * Date: 05.04.17
 * Time: 16:07
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation\Timestampable as Timestamp;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Table(name="Orders")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OrderRepository")
 */
class Order
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    //Transient fields
    /**
     * @JMS\Type("string")
     *
     */
    private $fullName;
    /**
     * @JMS\Type("string")
     *
     */
    private $email;
    /**
     * @JMS\Type("string")
     *
     */
    private $phone;


    /**
     *
     *
     * @ORM\ManyToOne(targetEntity="User", cascade={"refresh"})
     * @ORM\JoinColumn(name="user", referencedColumnName="id")
     *
     *
     */
    private $user;


    /**
     * @ORM\Column(type="string")
     */
    private $state;


    /**
     * @var \DateTime $created
     *
     * @Timestamp(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @ORM\Column(type="string")
     */
    private $mainType;

    /**
     * @ORM\Column(type="string")
     */
    private $typeOfManufacture;

    /**
     * @ORM\Column(type="boolean")
     *
     */
    private $needTooling;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     */
    private $toolingType;

    /**
     * @ORM\Column(type="integer", nullable=true)
     *
     */
    private $picsCount;


    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $sealType;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $sealShape;

    /**
     * @ORM\Column(type="boolean")
     */
    private $needIntegrateLogo;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $sealFIO;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $sealCompanyName;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $sealINN;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $sealOGRN;


    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $sealCity;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $sealText1;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $sealText2;


    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $size;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $height;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $width;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $stampText;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $deliveryType;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $needPrototype;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isFetching;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $count;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $paymentType;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $comment;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isASAP;


    /**
     * @return String
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * @param String $fullName
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getMainType()
    {
        return $this->mainType;
    }

    /**
     * @param mixed $mainType
     */
    public function setMainType($mainType)
    {
        $this->mainType = $mainType;
    }

    /**
     * @return mixed
     */
    public function getTypeOfManufacture()
    {
        return $this->typeOfManufacture;
    }

    /**
     * @param mixed $typeOfManufacture
     */
    public function setTypeOfManufacture($typeOfManufacture)
    {
        $this->typeOfManufacture = $typeOfManufacture;
    }

    /**
     * @return mixed
     */
    public function getNeedTooling()
    {
        return $this->needTooling;
    }

    /**
     * @param mixed $needTooling
     */
    public function setNeedTooling($needTooling)
    {
        $this->needTooling = $needTooling;
    }

    /**
     * @return String
     */
    public function getToolingType()
    {
        return $this->toolingType;
    }

    /**
     * @param String $toolingType
     */
    public function setToolingType($toolingType)
    {
        $this->toolingType = $toolingType;
    }

    /**
     * @return mixed
     */
    public function getPicsCount()
    {
        return $this->picsCount;
    }

    /**
     * @param mixed $picsCount
     */
    public function setPicsCount($picsCount)
    {
        $this->picsCount = $picsCount;
    }

    /**
     * @return mixed
     */
    public function getSealType()
    {
        return $this->sealType;
    }

    /**
     * @param mixed $sealType
     */
    public function setSealType($sealType)
    {
        $this->sealType = $sealType;
    }

    /**
     * @return mixed
     */
    public function getSealShape()
    {
        return $this->sealShape;
    }

    /**
     * @param mixed $sealShape
     */
    public function setSealShape($sealShape)
    {
        $this->sealShape = $sealShape;
    }

    /**
     * @return mixed
     */
    public function getNeedIntegrateLogo()
    {
        return $this->needIntegrateLogo;
    }

    /**
     * @param mixed $needIntegrateLogo
     */
    public function setNeedIntegrateLogo($needIntegrateLogo)
    {
        $this->needIntegrateLogo = $needIntegrateLogo;
    }

    /**
     * @return mixed
     */
    public function getSealFIO()
    {
        return $this->sealFIO;
    }

    /**
     * @param mixed $sealFIO
     */
    public function setSealFIO($sealFIO)
    {
        $this->sealFIO = $sealFIO;
    }

    /**
     * @return mixed
     */
    public function getSealCompanyName()
    {
        return $this->sealCompanyName;
    }

    /**
     * @param mixed $sealCompanyName
     */
    public function setSealCompanyName($sealCompanyName)
    {
        $this->sealCompanyName = $sealCompanyName;
    }

    /**
     * @return mixed
     */
    public function getSealINN()
    {
        return $this->sealINN;
    }

    /**
     * @param mixed $sealINN
     */
    public function setSealINN($sealINN)
    {
        $this->sealINN = $sealINN;
    }

    /**
     * @return mixed
     */
    public function getSealOGRN()
    {
        return $this->sealOGRN;
    }

    /**
     * @param mixed $sealOGRN
     */
    public function setSealOGRN($sealOGRN)
    {
        $this->sealOGRN = $sealOGRN;
    }

    /**
     * @return mixed
     */
    public function getSealCity()
    {
        return $this->sealCity;
    }

    /**
     * @param mixed $sealCity
     */
    public function setSealCity($sealCity)
    {
        $this->sealCity = $sealCity;
    }

    /**
     * @return mixed
     */
    public function getSealText1()
    {
        return $this->sealText1;
    }

    /**
     * @param mixed $sealText1
     */
    public function setSealText1($sealText1)
    {
        $this->sealText1 = $sealText1;
    }

    /**
     * @return mixed
     */
    public function getSealText2()
    {
        return $this->sealText2;
    }

    /**
     * @param mixed $sealText2
     */
    public function setSealText2($sealText2)
    {
        $this->sealText2 = $sealText2;
    }

    /**
     * @return mixed
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param mixed $size
     */
    public function setSize($size)
    {
        $this->size = $size;
    }

    /**
     * @return mixed
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param mixed $height
     */
    public function setHeight($height)
    {
        $this->height = $height;
    }

    /**
     * @return mixed
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param mixed $width
     */
    public function setWidth($width)
    {
        $this->width = $width;
    }

    /**
     * @return mixed
     */
    public function getStampText()
    {
        return $this->stampText;
    }

    /**
     * @param mixed $stampText
     */
    public function setStampText($stampText)
    {
        $this->stampText = $stampText;
    }

    /**
     * @return mixed
     */
    public function getDeliveryType()
    {
        return $this->deliveryType;
    }

    /**
     * @param mixed $deliveryType
     */
    public function setDeliveryType($deliveryType)
    {
        $this->deliveryType = $deliveryType;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getNeedPrototype()
    {
        return $this->needPrototype;
    }

    /**
     * @param mixed $needPrototype
     */
    public function setNeedPrototype($needPrototype)
    {
        $this->needPrototype = $needPrototype;
    }

    /**
     * @return mixed
     */
    public function getIsFetching()
    {
        return $this->isFetching;
    }

    /**
     * @param mixed $isFetching
     */
    public function setIsFetching($isFetching)
    {
        $this->isFetching = $isFetching;
    }

    /**
     * @return mixed
     */
    public function getIsOk()
    {
        return $this->isOk;
    }

    /**
     * @param mixed $isOk
     */
    public function setIsOk($isOk)
    {
        $this->isOk = $isOk;
    }

    /**
     * @return mixed
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param mixed $count
     */
    public function setCount($count)
    {
        $this->count = $count;
    }

    /**
     * @return mixed
     */
    public function getPaymentType()
    {
        return $this->paymentType;
    }

    /**
     * @param mixed $paymentType
     */
    public function setPaymentType($paymentType)
    {
        $this->paymentType = $paymentType;
    }

    /**
     * @return mixed
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param mixed $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return mixed
     */
    public function getIsASAP()
    {
        return $this->isASAP;
    }

    /**
     * @param mixed $isASAP
     */
    public function setIsASAP($isASAP)
    {
        $this->isASAP = $isASAP;
    }

    /**
     * @return mixed
     */
    public function getFinalPrice()
    {
        return $this->finalPrice;
    }

    /**
     * @param mixed $finalPrice
     */
    public function setFinalPrice($finalPrice)
    {
        $this->finalPrice = $finalPrice;
    }


    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }





}