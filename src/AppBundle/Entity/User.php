<?php

namespace AppBundle\Entity;

use AppBundle\Api\Model\Role;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\Groups;
use AppBundle\Api\Model\UserStatus;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Serializer;
use Gedmo\Mapping\Annotation\Timestampable as Timestamp;

/**
 * @ORM\Table(name="Users")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 *
 */
//TODO Maybe it's good idea to move to FOSUserBundle
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", unique=true)
     */
    private $username;


    /**
     * @ORM\Column(type="string")
     */
    private $fullName;

    /**
     * @ORM\Column(type="string")
     */
    private $role;


    /**
     * @ORM\Column(type="string")
     */
    private $phoneNumber;

    /**
     * @ORM\Column(type="string")
     */
    private $status;


    /**
     * @ORM\Column(type="string", unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string")
     * @Groups({"INCOMING_ONLY"})
     */
    private $password;


    /**
     * @var \DateTime $created
     *
     * @Timestamp(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;


    /**
     * @var \DateTime $updated
     *
     * @Timestamp(on="update")
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated;



    public static function createFromUnconfirmedUser(UnconfirmedUser $unconfirmedUser){
        $user = new User();
        $user->setEmail($unconfirmedUser->getEmail());
        $user->setPassword($unconfirmedUser->getPassword());
        $user->setPhoneNumber($unconfirmedUser->getPhoneNumber());
        $user->setFullname($unconfirmedUser->getFullName());
        $user->setUsername($unconfirmedUser->getUsername());
        $user->setCreated(new \DateTime());
        $user->setRole(Role::USER);
        $user->setStatus(UserStatus::READY);

        return $user;
    }

    /**
     * @return String
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param String $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }




    public function getId()
    {
        return $this->id;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param mixed $phoneNumber
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
    }


    public function getFullName()
    {
        return $this->fullName;
    }

    public function setFullname($fullName)
    {
        $this->fullName = $fullName;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return String
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param String $role
     */
    public function setRole($role)
    {
        $this->role = $role;
    }


    public function getRoles()
    {

        return array($this->getRole());

    }


    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }


    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated(\DateTime $updated)
    {
        $this->updated = $updated;
    }



    public function getSalt()
    {
        return;
    }

    public function eraseCredentials()
    {
    }
}
