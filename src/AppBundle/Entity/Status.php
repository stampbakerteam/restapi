<?php
/**
 * Created by PhpStorm.
 * User: lex
 * Date: 11.04.17
 * Time: 18:10
 */

namespace AppBundle\Entity;


class Status
{
    public $status = false;

    /**
     * RegistrationStatus constructor.
     * @param bool $status
     */
    public function __construct($status)
    {
        $this->status = $status;
    }


}