<?php
/**
 * Created by PhpStorm.
 * User: lex
 * Date: 06.04.17
 * Time: 13:44
 */

namespace AppBundle\Subscriber;


use AppBundle\Annotation\DeserializableType;
use AppBundle\Annotation\QueryParams;
use AppBundle\Api\Exception\ApiProblem;
use AppBundle\Api\Exception\ApiProblemException;
use AppBundle\AppBundle;
use AppBundle\Entity\Order;
use AppBundle\Service\AbstractService;
use Doctrine\Common\Annotations\Reader;
use Doctrine\ORM\EntityManager;
use JMS\Serializer\DeserializationContext;
use JMS\Serializer\EventDispatcher\ObjectEvent;
use JMS\Serializer\SerializationContext;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\FilterControllerArgumentsEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Event\KernelEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class FilterSubscriber extends AbstractService implements EventSubscriberInterface
{


    public function __construct(ContainerInterface $container)
    {

        $this->setContainer($container);


    }


    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * array('eventName' => 'methodName')
     *  * array('eventName' => array('methodName', $priority))
     *  * array('eventName' => array(array('methodName1', $priority), array('methodName2')))
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return array(
            KernelEvents::CONTROLLER_ARGUMENTS => array(
                array('deserializeRequest', 0),
                array('bindQueryParams', 10),
            ),

            KernelEvents::EXCEPTION => array(
                array('onKernelException'),

            ),

        );
    }

    //TODO Handle header parameters
    public function bindQueryParams(FilterControllerArgumentsEvent $event)
    {
        $controller = $event->getController();
        $args = $event->getArguments();
        if (!is_array($controller)) {
            return;
        }
        $queryParamsBindings = $this->getContainer()->get('api.annotation_handler')->getAnnotation($event, QueryParams::class);
        if (!$queryParamsBindings) {
            return;
        }


        $queryParams = $event->getRequest()->query->all();
        $controllerParamIndex = ($event->getRequest()->getContent()) ? 1 : 0;

        foreach ($queryParamsBindings->params as $queryParamNameBinding => $controllerArgNameBinding) {
            foreach ($queryParams as $queryParamName => $queryParamValue) {
                if ($queryParamNameBinding == $queryParamName) {
                    $args[$controllerParamIndex] = $queryParamValue;
                    $controllerParamIndex++;
                }
            }
        }
        $event->setArguments($args);
    }


    public function deserializeRequest(FilterControllerArgumentsEvent $event)
    {
        $controller = $event->getController();
        $args = $event->getArguments();
        if (!is_array($controller)) {
            return;
        }
        $content = $event->getRequest()->getContent();
        $expectingType = $this->getContainer()->get('api.annotation_handler')->getAnnotation($event, DeserializableType::class);
        if ($content && $expectingType->type != DeserializableType::NONE) {

            if ($expectingType === null) {
                $apiProblem = new ApiProblem(500, ApiProblem::TYPE_NOT_FOUND_DESERIALIZATION_TYPE);
                throw new ApiProblemException($apiProblem);
            }


            $contentType = $event->getRequest()->getContentType();
            //TODO Add another types;
            if (strcasecmp($contentType, 'xml') == 0) {
                $format = 'xml';

            } else {
                $format = 'json';

            }
            $content = $this->getContainer()->get('jms_serializer')->deserialize($content, $expectingType->type, $format);
            if ($content === null) {
                $apiProblem = new ApiProblem(400, ApiProblem::TYPE_SERIALIZATION_ERROR);
                throw new ApiProblemException($apiProblem);
            }
            $args[0] = $content;
        }

        $event->setArguments($args);

    }




    public function onKernelException(GetResponseForExceptionEvent $event)
    {


        $e = $event->getException();

        $statusCode = $e instanceof HttpExceptionInterface ? $e->getStatusCode() : 500;

        if ($e instanceof ApiProblemException) {
            $apiProblem = $e->getApiProblem();
        } else {


            $apiProblem = new ApiProblem(
                $statusCode
            );


            $apiProblem->set('detail', $e->getMessage());

        }

        $data = $apiProblem->toArray();


        $response = new JsonResponse(
            $data,
            $apiProblem->getStatusCode()
        );

        //TODO Make serialization dependent on the Accept header;
        $response->headers->set('Content-Type', 'application/problem+json');

        $event->setResponse($response);
    }

}