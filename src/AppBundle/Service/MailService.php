<?php
/**
 * Created by PhpStorm.
 * User: lex
 * Date: 11.04.17
 * Time: 19:01
 */

namespace AppBundle\Service;


use AppBundle\Entity\UnconfirmedUser;
use Swift_Mailer;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MailService extends AbstractService
{


    const SEND_FROM = "noreply@cashtrack.xyz";





    public function sendRegistrationConfirmation(UnconfirmedUser $user)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject('Регистрация на StampBaker')
            ->setFrom(MailService::SEND_FROM)
            ->setTo($user->getEmail())
            ->setBody(
                $this->getContainer()->get('templating')->render(
                    'emails/registration.html.twig',
                    array('username' => $user->getUsername(),
                        'hash' => $user->getHash())
                ),
                'text/html'
            );
        $this->getContainer()->get('mailer')->send($message);
    }

}