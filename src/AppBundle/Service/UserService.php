<?php
/**
 * Created by PhpStorm.
 * User: lex
 * Date: 05.04.17
 * Time: 14:49
 */

namespace AppBundle\Service;


use AppBundle\Api\Exception\ApiProblem;
use AppBundle\Api\Exception\ApiProblemException;
use AppBundle\Api\Model\UserStatus;
use AppBundle\Entity\User;
use AppBundle\Repository\UserRepository;
use Doctrine\ORM\Repository\DefaultRepositoryFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;

class UserService extends AbstractService
{


    public function loadUserByCredential(String $credential)
    {
        $users = $this->getContainer()->get('api.user_repository')->findByEmailOrUsername($credential, $credential);
        $user = null;
        if (count($users) > 0) {
            $user = $users[0];
        }
        return $user;
    }

    public function checkUserExistence($email, $username){
        $existingUsers = $this->getContainer()->get('api.user_repository')->findByEmailOrUsername($email, $username);
        return count($existingUsers)> 0;
    }


    public function createUser(User $user)
    {
        //TODO add normal validation
        $apiProblem = new ApiProblem(400, ApiProblem::TYPE_VALIDATION_ERROR);
        if (!$user->getEmail()) {
            $apiProblem->set('email', 'email is required');
        }
        if (!$user->getFullName()) {

            $apiProblem->set('fullname', 'fullname is required');
        }
        if (!$user->getPassword()) {
            $apiProblem->set('password', 'password is required');
        }

        $existence = $this->checkUserExistence($user->getEmail(), $user->getUsername());

        if ($existence) {
            $apiProblem->set('email or username', 'A user with this email or username is already registered');
        }


        if (count($apiProblem->getExtraData()) > 0) {

            throw new ApiProblemException($apiProblem);
        }

        $user->setCreated(new \DateTime());

        $encodedPassword = $this->getContainer()->get('security.password_encoder')
            ->encodePassword($user, $user->getPassword());
        $user->setPassword($encodedPassword);
        $user->setStatus(UserStatus::READY);

        $this->getContainer()->get('api.user_repository')->save($user);

        return $user;


    }

    public function getMe()
    {
        return $this->getContainer()->get('api.jwt_token_authenticator')->getCurrentUser();
    }


    public function findAll()
    {
        return $this->getContainer()->get("api.user_repository")->findAll();
    }


    public function findById($id)
    {
        $user = $this->getContainer()->get("api.user_repository")->findById($id);
        if (!$user) {
            throw new ApiProblemException(new ApiProblem(404, ApiProblem::TYPE_NOT_FOUND));
        }
        return $user;
    }

    public function deleteUser($id)
    {
        $user = $this->findById($id);
        $this->getContainer()->get("api.user_repository")->delete($user);
    }

    public function banUser($id)
    {
        $user = $this->findById($id);
        $user->setStatus(UserStatus::BANNED);
        $this->getContainer()->get("api.user_repository")->update($user);

    }

    public function restoreUser($id)
    {
        $user = $this->findById($id);
        $user->setStatus(UserStatus::READY);
        $this->getContainer()->get("api.user_repository")->update($user);
    }

    //TODO add validation
    public function updateUser(User $user)
    {
        if (!$user->getId()) {
            throw new ApiProblemException(new ApiProblem(400, ApiProblem::TYPE_VALIDATION_ERROR));
        }
        $encodedPassword = $this->getContainer()->get('security.password_encoder')
            ->encodePassword($user, $user->getPassword());
        $user->setPassword($encodedPassword);
        $user->setUpdated(new \DateTime());
        $this->getContainer()->get("api.user_repository")->update($user);
        return $user;
    }

    //TODO Make method handles email/username hot validation

}