<?php
/**
 * Created by PhpStorm.
 * User: lex
 * Date: 13.04.17
 * Time: 13:04
 */

namespace AppBundle\Service;


use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class AbstractService
{

    private $container;

    /**
     * @return ContainerInterface
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @param ContainerInterface $container
     */
    public function setContainer($container)
    {
        $this->container = $container;
    }




}