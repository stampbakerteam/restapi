<?php
/**
 * Created by PhpStorm.
 * User: lex
 * Date: 10.04.17
 * Time: 18:04
 */

namespace AppBundle\Service;


use AppBundle\Api\Exception\ApiProblem;
use AppBundle\Api\Exception\ApiProblemException;
use AppBundle\Api\Model\Credentials;
use AppBundle\Api\Model\UserStatus;
use AppBundle\Entity\Status;
use AppBundle\Entity\UnconfirmedUser;
use AppBundle\Entity\User;


class AuthService extends AbstractService
{


    public function auth(Credentials $credentials)
    {
        $user = $this->getContainer()->get('api.user_repository')->findUserByEmail($credentials->getUsername());
        if (!$user) {
            $user = $this->getContainer()->get('api.user_repository')->findUserByUsername($credentials->getUsername());
        }
        if (!$user) {
            throw new ApiProblemException(new ApiProblem(403, ApiProblem::TYPE_AUTH_FAILED));
        }
        if (!$this->getContainer()->get('security.password_encoder')->isPasswordValid($user, $credentials->getPassword())) {
            throw new ApiProblemException(new ApiProblem(403, ApiProblem::TYPE_AUTH_FAILED));
        }
        if ($user->getStatus() == UserStatus::BANNED) {
            throw new ApiProblemException(new ApiProblem(403, ApiProblem::TYPE_BANNED));
        }
        return $this->getContainer()->get('api.jwt_token_authenticator')->createToken($user);


    }

    //TODO Make service method to resend confirmation


    public function register(UnconfirmedUser $user)
    {   //TODO Make validation on fullname, password etc.

        if (count($this->getContainer()->get('api.user_repository')->findByEmailOrUsername($user->getEmail(), $user->getUsername())) > 0) {
            $apiProblem = new ApiProblem(400, ApiProblem::TYPE_VALIDATION_ERROR);
            $apiProblem->set("message", "User already exist with same credentials");
            throw new ApiProblemException($apiProblem);
        } else {

            if (count($this->getContainer()->get('api.unconfirmed_user_repository')->findByEmailOrUsername($user->getEmail(), $user->getUsername())) > 0) {
                //Offer to the user to resent the email
                return new Status(false);
            } else {
                //TODO Remove to normal hash
                $user->setHash(uniqid('', true));
                $user->setCreated(new \DateTime());

                $encodedPassword = $this->getContainer()->get('security.password_encoder')
                    ->encodePassword($user, $user->getPassword());
                $user->setPassword($encodedPassword);
                $this->getContainer()->get('api.unconfirmed_user_repository')->save($user);
                $this->getContainer()->get('api.mail_service')->sendRegistrationConfirmation($user);
                return new Status(true);
            }
        }
    }


    public function confirmRegistration($hash)
    {
        $unconfirmedUser = $this->getContainer()->get('api.unconfirmed_user_repository')->getByHash($hash);
        if (!$unconfirmedUser) {
            $apiProblem = new ApiProblem(404, ApiProblem::TYPE_REG_HASH_DOESNT_FOUND);
            throw new ApiProblemException($apiProblem);
        }
        $newUser = User::createFromUnconfirmedUser($unconfirmedUser);

        $this->getContainer()->get('api.unconfirmed_user_repository')->delete($unconfirmedUser);

        $this->getContainer()->get('api.user_repository')->save($newUser);

        $this->getContainer()->get('doctrine.orm.entity_manager')->flush();

        return $this->getContainer()->get('api.jwt_token_authenticator')->createToken($newUser);


    }


}