<?php
/**
 * Created by PhpStorm.
 * User: lex
 * Date: 05.04.17
 * Time: 14:50
 */

namespace AppBundle\Service;


use AppBundle\Api\Exception\ApiProblem;
use AppBundle\Api\Exception\ApiProblemException;
use AppBundle\Api\Model\OrderState;
use AppBundle\Api\Model\Role;
use AppBundle\Api\Model\UserStatus;
use AppBundle\Entity\Order;
use AppBundle\Entity\User;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Yaml\Tests\A;

class OrderService extends AbstractService
{

    public function getAllOrders()
    {
        return $this->getContainer()->get('api.order_repository')->getAll();
    }

    public function getMyOrders()
    {
        $user = $this->getContainer()->get('api.jwt_token_authenticator')->getCurrentUser();
        return $this->getContainer()->get('api.order_repository')->findByUserId($user->getId());
    }


    public function findByIdOrders($id)
    {
        if (!$id) {
            throw new ApiProblemException(new ApiProblem(400, ApiProblem::TYPE_VALIDATION_ERROR));
        }
        $orders = $this->getContainer()->get('api.order_repository')->findById($id);

        if (!$orders) {
            throw new ApiProblemException(new ApiProblem(404, ApiProblem::TYPE_NOT_FOUND));
        }
        return $orders;
    }

    //TODO Add validation
    public function createOrder(Order $order)
    {
        //if order is anonymous -> creating new users from info in order
        if (!$order->getUser()) {

            $user = $this->getContainer()->get('api.user_service')->loadUserByCredential($order->getEmail());
            if (!$user) {
                $user = new User();
                $user->setEmail($order->getEmail());
                $user->setUsername($order->getEmail());
                $user->setFullname($order->getFullName());
                $user->setPhoneNumber($order->getPhone());
                $user->setRole(Role::USER);
                $user->setCreated(new \DateTime());
                $user->setStatus(UserStatus::READY);
                $this->getContainer()->get('hackzilla.password_generator.dummy')->setLength(6);
                $newPassword = $this->getContainer()->get('hackzilla.password_generator.dummy')->generatePassword();
                $user->setPassword($this->getContainer()->get('security.password_encoder')->encodePassword($user, $newPassword));

                $this->getContainer()->get('api.user_service')->createUser($user);
                $this->getContainer()->get('doctrine.orm.entity_manager')->flush();

                //TODO Send email about registration for user;

            }

            $order->setUser($user);



        }else{
            //Вопрос к знатокам - существует возможность связывать объекты с ORM кроме как через выборку из базы или persit? ну способ выполнения выборки неявно?)
            $order->setUser($this->getContainer()->get('api.user_service')->findById($order->getUser()));
        }


        $order->setState(OrderState::CREATED);
        $order->setCreated(new \DateTime());
        $this->getContainer()->get('api.order_repository')->save($order);
        return $order;
        //TODO Notify user about new order by email;
        //TODO Notify admins about new order by email and telegram;


    }

    public function deleteOrder($id)
    {
        $this->updateOrdersState($id, OrderState::DELETED);
    }

    public function startProcessOrder($id)
    {
        $this->updateOrdersState($id, OrderState::IN_PROGRESS);
    }

    public function closeOrder($id)
    {
        $this->updateOrdersState($id, OrderState::DONE);
    }

    private function updateOrdersState($id, $state)
    {
        if (!$id) {
            throw new ApiProblemException(new ApiProblem(400, ApiProblem::TYPE_VALIDATION_ERROR));
        }


        $foundOrder = $this->getContainer()->get('api.order_repository')->findById($id);


        $foundOrder->setState($state);
        $this->getContainer()->get("api.order_repository")->update($foundOrder);
    }
}