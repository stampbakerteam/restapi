<?php
/**
 * Created by PhpStorm.
 * User: lex
 * Date: 13.04.17
 * Time: 13:31
 */

namespace AppBundle\Annotation\handler;


use AppBundle\Service\AbstractService;
use ReflectionClass;
use function Sodium\add;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;
use Symfony\Component\HttpKernel\Event\KernelEvent;

class AnnotationHandler extends AbstractService
{
    //TODO Dirty hack!, fix it ASAP
    public function getAnnotation(KernelEvent $event, String $annotationType)
    {
        $controllerResolver = new ControllerResolver();
        $controllers = $controllerResolver->getController($event->getRequest());
        if (!is_array($controllers)) {
            return;
        }
        $params = explode('::', $event->getRequest()->attributes->get('_controller'));
        $annotations = $this->getContainer()->get('annotation_reader')
            ->getMethodAnnotations(new \ReflectionMethod($controllers[0], $params[1]));

        return $this->findAnnotation($annotations, $annotationType);

    }

    private function findAnnotation($annotations, $annotationType)
    {
        foreach ($annotations as $annotation) {
            if ($annotation instanceof $annotationType) {
                return $annotation;
            }
        }
    }

}