<?php
/**
 * Created by PhpStorm.
 * User: lex
 * Date: 10.04.17
 * Time: 14:14
 */

namespace AppBundle\Annotation;

/**
 * @Annotation
 * @Target("METHOD")
 */
class DeserializableType
{
    public $type;


    const NONE = 'NONE';
}