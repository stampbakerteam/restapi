<?php
/**
 * Created by PhpStorm.
 * User: lex
 * Date: 12.04.17
 * Time: 14:22
 */

namespace AppBundle\Annotation;

/**
 * Binds query params with controller args;
 *
 * @Annotation
 * @Target("METHOD")
 */
class QueryParams
{
    public $params = array();
}