<?php
/**
 * Created by PhpStorm.
 * User: lex
 * Date: 13.04.17
 * Time: 16:18
 */

namespace AppBundle\Api\Serialization;


use JMS\Serializer\Context;
use JMS\Serializer\Exclusion\ExclusionStrategyInterface;
use JMS\Serializer\Metadata\ClassMetadata;
use JMS\Serializer\Metadata\PropertyMetadata;

class SerializationExclusionStrategy implements ExclusionStrategyInterface
{

    /**
     * Whether the class should be skipped.
     *
     * @param ClassMetadata $metadata
     *
     * @return boolean
     */
    public function shouldSkipClass(ClassMetadata $metadata, Context $context)
    {
        return false;
    }

    /**
     * Whether the property should be skipped.
     *
     * @param PropertyMetadata $property
     *
     * @return boolean
     */
    public function shouldSkipProperty(PropertyMetadata $property, Context $context)
    {
        if (!$property->groups){
            return false;
        }
        foreach ($property->groups as $group){
            if ($group == SerializationGroup::INCOMING_ONLY){
                return true;
            }
        }
        return false;
    }
}