<?php
/**
 * Created by PhpStorm.
 * User: lex
 * Date: 12.04.17
 * Time: 15:33
 */

namespace AppBundle\Api\Model;


abstract class Role
{
    const USER = "ROLE_USER";
    const ADMIN = "ROLE_ADMIN";

}