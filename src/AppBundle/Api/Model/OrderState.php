<?php
/**
 * Created by PhpStorm.
 * User: lex
 * Date: 13.04.17
 * Time: 18:15
 */

namespace AppBundle\Api\Model;


class OrderState
{
    const CREATED = 'CREATED';
    const IN_PROGRESS = 'IN_PROGRESS';
    const READY = 'READY';
    const DONE = 'DONE';
    const DELETED = 'DELETED';
}