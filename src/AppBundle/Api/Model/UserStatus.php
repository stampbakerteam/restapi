<?php
/**
 * Created by PhpStorm.
 * User: lex
 * Date: 13.04.17
 * Time: 12:19
 */

namespace AppBundle\Api\Model;


abstract class UserStatus
{
    const READY = "READY";
    const BANNED = "BANNED";
}