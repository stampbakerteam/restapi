<?php
/**
 * Created by PhpStorm.
 * User: lex
 * Date: 11.04.17
 * Time: 15:22
 */

namespace AppBundle\Api\Model;


class JWToken
{

    private $token;

    /**
     * JWToken constructor.
     * @param $token
     */
    public function __construct($token)
    {
        $this->token = $token;
    }


    /**
     * @return String
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param String $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }




}