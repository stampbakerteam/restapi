<?php
/**
 * Created by PhpStorm.
 * User: lex
 * Date: 12.04.17
 * Time: 15:51
 */

namespace AppBundle\Api\Model;


use AppBundle\Entity\User;
use JMS\Serializer\Annotation as JMS;

class Credentials
{


    /**
     * @JMS\Type("string")
     *
     */
    private $username;


    /**
     *
     *@JMS\Type("string")
     *
     */
    private $password;



    /**
     * @return String
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param String $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return String
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param String $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }




}